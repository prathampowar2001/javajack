package com.pratham.powar;
// This is the first step towards java jack 
import java.io.PrintWriter;
public class Startup{
	public Startup(){
		PrintWriter writer=null;
		try{
			if(!alreadyStartUpDone(){
				writer =new PrintWriter(".JavaJack.log","UTF-8");
				writer.println("This is an auto generated file and do not delete or edit it");
				writer.println("Successful startup");
				writer.println("S$1");
			}else{
				System.out.println("You have already completed this step put the right lesson code or type `javajack hardreset`");
			}
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Some error has happened during the creation of the test file");
			
		}finally{
			writer.close();
		}
		
	}
	public void introMessage(){

	}
	public void alreadyStartUpDone(){}
	

}

